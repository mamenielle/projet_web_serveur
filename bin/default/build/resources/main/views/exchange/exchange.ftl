<#ftl encoding="utf-8">
<head>
    <title>Echange</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
<body>
  <div class="container">
    <h1>Plateforme d'échange</h1>

    <h2>Proposer un échange</h2>
    <form method="post" action="/exchange">
      <label for="idPokemon1">Identifiant Pokémon du Pokémon que vous souhaitez échanger : </label>
      <input type="text" id="idPokemon1" name="idPokemon1" required>
      <br>
      <label for="idPokemon2">Identifiant Pokémon du Pokémon que vous aimeriez : </label>
      <input type="text" id="idPokemon2" name="idPokemon2" required>
      <br>
      <input type="submit" value="Valider" class="btn">
    </form>

    <h2>Accepter un échange</h2>
    <form method="post" action="/accept_exchange">
      <label for="idExchange">Identifiant de l'échange que vous souhaitez accepter : </label>
      <input type="text" id="idExchange" name="idExchange" required>
      <br>
      <input type="submit" value="Valider" class="btn">
    </form>

    <br>

    <p><a href="pokemons_for_exchange">Liste des pokémons que vous pouvez récupérer</a></p>

    <p>Cliquez <a href="/">ici</a> pour aller à l'accueil</p>
  </div>
  </body>
</html>