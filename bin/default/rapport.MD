Tables SQL : 

On crée d'abord la table liée à l'utilisateur :

```sql
CREATE TABLE IF NOT EXISTS User (
    idUser int PRIMARY KEY AUTO_INCREMENT, 
    firstname varchar(50), 
    lastname varchar(50), 
    email varchar(50), 
    password varchar(50), 
    lastConnection timestamp 
);
```

L'utilisation d'une date sous forme de `timestamp` nous permet d'avoir accès à la fois à la date et à l'heure.

On crée la table liée à l'espèce du pokémon :

```sql
CREATE TABLE IF NOT EXISTS Pokemon (
    idPokemon int PRIMARY KEY AUTO_INCREMENT, 
    idUser int REFERENCES User (idUser), 
    idAPI int, name varchar(50), 
    nickname varchar(50), 
    level int
);
```

Enfin on crée une table afin d'échanger des cartes.

```sql
CREATE TABLE IF NOT EXISTS ExchangeList (
    idExchange INT PRIMARY KEY AUTO_INCREMENT, 
    userA INT REFERENCES User(idUser), 
    userB INT REFERENCES User(idUser), 
    pokemonA INT REFERENCES Pokemon(idPokemon),  
    pokemonB INT REFERENCES Pokemon(idPokemon), 
    dateOfExchange DATE);
```