<#ftl encoding="utf-8">
<head>
    <title>Liste Echanges</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
  <body>
    <div class="container">
      <h1 style="text-align: center;">Liste des Echanges souhaités</h1>
      <ul>
        <#list exchanges as exchange>
          <li>Identifiant de l'échange : ${exchange.getIdExchange()}</li>
          <br>
          <li>Identifiant du Pokémon qui souhaite être échangé: ${exchange.getIdPokemon()}</li>
          <li>Identifiant de son maître : ${exchange.getIdUser()} </li>
          <br>
          <li>Identifiant du Pokémon souhaité : ${exchange.getIdPokemon2()} </li>
          <br>
          <hr>
          <br>
        </#list>
      </ul>
    </div>
  </body>
</html>