<#ftl encoding="utf-8">
<head>
    <title>Succés</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<body xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <p>C'est un succès !!!</p>
        <p><a href="/">Retour au menu</a></p>
    </div>
</body>

</html>
