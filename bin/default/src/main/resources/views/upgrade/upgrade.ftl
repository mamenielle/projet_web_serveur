<#ftl encoding="utf-8">

<head>
    <title>Succés</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<body xmlns="http://www.w3.org/1999/html">
  <div class="container">
      <h1>Améliorer un Pokémon</h1>
      <form method="post" action="/upgrade">
        <label for="idPokemon">Identifiant du Pokémon souhaité : </label>
        <input type="text" id="idPokemon" name="idPokemon" required>
        <br>
        <input type="submit" value="Valider" class="btn">
      </form>
  </div>
  </body>
</html>