<#ftl encoding="utf-8">
<head>
    <title>Erreur</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<body xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <p>L'identifiant pokémon entré ne correspond à aucune Pokéball.</p>
    </div>
</body>

</html>