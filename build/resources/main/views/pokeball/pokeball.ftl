<#ftl encoding="utf-8">
<head>
    <title>Pokéball</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
<body>
    <div class="container">
        <h1 style = "text-align: center;">Votre Pokéball</h1>
        <ul>
            <#list pokemons as pokemon>
                <li>Identifiant du Pokémon : ${pokemon.getIdPokemon()}</li>
                <li>Identifiant du Pokémon (API) : ${pokemon.getIdAPI()} </li>
                <li>Identifiant de son maître : ${pokemon.getIdMaster()} </li>
                <li>Nom du Pokémon : <strong>${pokemon.getName()}</strong> </li>
                <li>Surnom du Pokémon : ${pokemon.getNickname()} </li>
                <li>Expérience : ${pokemon.getLevel()}</li>
                <br>
                <hr>
                <br>
            </#list>
        </ul>
    </div>
</body>
</html>