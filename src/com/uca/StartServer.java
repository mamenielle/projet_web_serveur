package com.uca;

import com.uca.dao._Initializer;
import com.uca.core.*;
import com.uca.gui.*;
import com.uca.entity.*;
import com.uca.entity.UserEntity;
import com.uca.dao.UserDAO;
import com.uca.dao.PokemonDAO;

import static spark.Spark.*; 

/* imports pour sécuriser authentification */

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import spark.Request;
import spark.Response;
import spark.Route;

import java.security.SecureRandom;
import java.util.Base64;

/* --- */

public class StartServer {

    private static final String SECRET_KEY = generateSecretKey(); // clé secrète authentification

    public static void main(String[] args) {
        //Configure Spark
        staticFiles.location("/static/");
        port(8081);


        _Initializer.Init();


        // accueil

        get("/", (req, res) -> {
            return HomeGUI.displayHome();
        });


        // utilisateurs

        get("/users", (req, res) -> {
            String cookie = req.cookie("auth");
            
            if (cookie == null){
                res.redirect("/login");
                return null;
            }

            return UserGUI.getAllUsers();
        });


        //pokémon liés à un utlisateur spécifique (sa pokéball)

        get("/pokeball", (req, res) -> {
            String cookie = req.cookie("auth");
            
            if (cookie == null){
                res.redirect("/login");
                return null;
            }
            
            int idUser = readIdFromToken(cookie);
            return PokemonGUI.getPokeball(idUser);
        });

        // pokémons des autres joueurs

        get("/pokemons_for_exchange", (req, res) -> {
            String cookie = req.cookie("auth");
            
            if (cookie == null){
                res.redirect("/login");
                return null;
            }
            
            int idUser = readIdFromToken(cookie);
            return PokemonGUI.getPokemonsForExchange(idUser);
        });

        // améliorer/pexer un pokemon
        
        get("/upgrade", (req, res) -> {
            String cookie = req.cookie("auth");
            
            if (cookie == null){
                res.redirect("/login");
                return null;
            }

            return UserGUI.loadFormUpgrade();
        });

        post("/upgrade", (req, res) -> {
            String i = req.queryParams("idPokemon");
            int id;

            try {
                id = Integer.parseInt(i);
            } catch (NumberFormatException e) {
                return UserGUI.displayFailUpdate();
            }

            int upgradeCompleted;

            // Récupérer les informations du Pokemon
            PokemonDAO pokemonDAO = new PokemonDAO();
            upgradeCompleted = pokemonDAO.upgradePokemon(id);

            if(upgradeCompleted == 1){
                return UserGUI.displaySuccessUpgrade();
            }
            else{
                return UserGUI.displayFailUpgrade(); 
            }
        });

        // changer le surnom d'un pokémon

        get("/nickname", (req, res) -> {
            String cookie = req.cookie("auth");
            
            if (cookie == null){
                res.redirect("/login");
                return null;
            }

            return UserGUI.loadFormNickname();
        });

        post("/nickname", (req, res) -> {
            String i = req.queryParams("idPokemon");
            String nickname = req.queryParams("newNickname");
            
            int id;
            try {
                id = Integer.parseInt(i);
            } catch (NumberFormatException e) {
                return UserGUI.displayFailUpdate();
            }

            String cookie = req.cookie("auth");
            
            if (cookie == null){
                res.redirect("/login");
                return null;
            }
            
            int idUser = readIdFromToken(cookie);

            int updateCompleted;

            // Récupérer les informations du Pokemon
            PokemonDAO pokemonDAO = new PokemonDAO();
            updateCompleted = pokemonDAO.updateNickname(id, nickname, idUser);

            if(updateCompleted == 1){
                return UserGUI.displaySuccessUpdate();
            }
            else{
                return UserGUI.displayFailUpdate(); 
            }
        });
 

        // inscription (mais on n'est pas connecté lorsque l'on s'inscrit)

        get("/register", (req, res) -> {
            return UserGUI.loadForm();
        });

        post("/register", (req, res) -> {

            String firstname = req.queryParams("firstname");
            String lastname = req.queryParams("lastname");
            String email = req.queryParams("email");
            String password = req.queryParams("password");

            UserEntity u = new UserEntity();
            u.setFirstName(firstname);
            u.setLastName(lastname);
            u.setEmail(email);
            u.setPassword(password);

            // UserDAO userDAO = new UserDAO(); // création d'une instance de UserDAO
            UserEntity b = UserCore.create(u); // appel de la méthode create avec l'objet UserEntity
            
            if(b == null){ //cas où user existe déjà 
                return UserGUI.displayFail();
            }

            PokemonCore.getNewPokemon(b.getIdUser());
            
            return UserGUI.displaySuccess();
        });

        // connexion

        get("/login", (req, res) -> {
            return UserGUI.loadFormLogIn();
        });
    
        post("/login", (req, res) -> {
            String email = req.queryParams("email");
            String password = req.queryParams("password");
            // boolean rememberMe = req.queryParams("rememberMe") != null;
            
            UserDAO userDAO = new UserDAO();
            int id = userDAO.findByEmailAndPassword(email, password);
    
            if (id >= 0) {
                String cookie = req.cookie("auth"); 

                if (cookie == null){ // cas où personne n'est connecté

                    // Génération d'un token d'authentification
                    String authToken = generateToken(id);
                    // Ajout du cookie d'authentification
                    res.cookie("auth", authToken, 864000); // dure un jour max
                    
                    UserDAO u1 = new UserDAO();
                    boolean b1 = u1.getDailyPokemon(readIdFromToken(authToken)); // récupération du pokémon toutes les 24 h

                    res.redirect("/"); // redirection vers la page d'accueil dans le cas où l'authentification a fonctionné
                }
                else if (!(readIdFromToken(cookie) == id)){ // cas où qqn d'autre est déjà déconnecté
                    res.redirect("/logout");
                }
                else{
                    // Redirection de l'utilisateur vers sa page d'accueil
                    res.redirect("/logged");
                }
                
            } else if (id == -1){ // pas de compte associé à l'email
                res.redirect("/register");
            } else if (id == -2){ // mauvais mdp
                res.redirect("/login"); 
            } else {
                res.status(500);
            }

            return res;
        });

        // deconnexion

        get("/logout", (req, res) -> {
            return UserGUI.loadFormLogOut();
        });

        post("/logout", (req, res) -> {
            res.removeCookie("auth");
            return UserGUI.loadFormLogIn();
        });

        // affichage de "déjà connecté"

        get("/logged", (req, res) -> {
            return UserGUI.loadAlreadyLogged();
        });


        // platerforme d'échange

        get("/exchange", (req, res) -> {
            String cookie = req.cookie("auth");
            if (cookie == null){
                res.redirect("/login");
                return null;
            }

            return ExchangeGUI.loadExchangeForm();
        });

        // proposer un échange 

        post("/exchange", (req, res) -> {

            String cookie = req.cookie("auth");
            if (cookie == null){
                res.redirect("/login");
                return null;
            }

            String i1 = req.queryParams("idPokemon1");
            String i2 = req.queryParams("idPokemon2"); 
    
            int id1;
            try {
                id1 = Integer.parseInt(i1);
            } catch (NumberFormatException e) {
                return UserGUI.displayFailUpdate();
            }
    
            int id2;
            try {
                id2 = Integer.parseInt(i2);

            } catch (NumberFormatException e) {
                return UserGUI.displayFailUpdate();
            }

            ExchangeEntity e = new ExchangeEntity(readIdFromToken(cookie), id1, id2);
            ExchangeEntity exchange = ExchangeCore.create(e);

            if(exchange == null){
                // cas où rien n'est créé
                // exemple : cas où l'utilisateur ne possède pas le pokémon à échanger
                res.redirect("/exchange");
            }
            else{ 
                // cas où ça a fonctionné
                res.redirect("/exchangeList");
            }
            return null;
        });

        // accepter un échange

        post("/accept_exchange", (req, res) -> {

            String cookie = req.cookie("auth");
            if (cookie == null){
                res.redirect("/login");
                return null;
            }

            String i = req.queryParams("idExchange");
    
            int id;
            try {
                id = Integer.parseInt(i);
            } catch (NumberFormatException e) {
                return UserGUI.displayFailUpdate();
            }
    
            ExchangeEntity e = new ExchangeEntity(readIdFromToken(cookie), id);
            e = new ExchangeCore().getInformationExchange(id);

            if(e == null){
                // cas où l'échange n'a pas fonctionné par exemple lorsque l'id entré ne correspond à aucun idExchange de Exchange
                res.redirect("/exchange");
            }

            boolean b = ExchangeCore.updateIdUsers(e, readIdFromToken(cookie));
            if(!b){ // cas où l'échange n'a pas fonctionné
                res.redirect("/exchange");
            }
            else{ // s'il a fonctionné, on le supprime de la liste
                ExchangeCore.delete(e, readIdFromToken(cookie));
                res.redirect("/exchangeList");
            }

            return null;
        });


        // voir la liste des échanges

        get("/exchangeList", (req, res) -> {
            return ExchangeGUI.getExchangeList();
        });

    }

    private static String generateSecretKey() {
        byte[] keyBytes = new byte[32]; // 256 bits
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(keyBytes);
        return Base64.getEncoder().encodeToString(keyBytes);
    }


    // création d'un token d'authentification

    private static String generateToken(int id) {
        // Décode la clé secrète en tableau de bytes
        byte[] keyBytes = Base64.getDecoder().decode(SECRET_KEY);
        
        // Crée un token JWT en utilisant l'algorithme de signature HMAC-SHA256
        return Jwts.builder()
                // definit l'id du token en tant que chaîne représentant l'ID passé en argument
                .setId(String.valueOf(id))
                // Signe le token en utilisant la clé secrète et l'algorithme HMAC-SHA256
                .signWith(Keys.hmacShaKeyFor(keyBytes), SignatureAlgorithm.HS256)
                // le token JWT, qui est initialement représenté sous forme d'objet, est transformé en String
                .compact();
    }    

    // décryptage du token

    private static int readIdFromToken(String token) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(SECRET_KEY) // spécifie la clé secrète
                    .build() // construit le parseur de token
                    .parseClaimsJws(token) // analyse le token pour extraire les informations (id)
                    .getBody();
                    
            // récupération de l'id à partir des claims du token
            return Integer.parseInt(claims.getId());
        } catch (Exception e) {
            return -1;
        }
    }
    
    
}