package com.uca.core;

import com.uca.dao.ExchangeDAO;
import com.uca.entity.ExchangeEntity;

import java.util.ArrayList;

public class ExchangeCore {

    /* cette fonction récupère toutes les lignes de la table Exchange (utilisation de la technique 
    d'ORM avec les valeurs qui sont insérées dans des instances de ExchangeEntity) */

    public static ArrayList<ExchangeEntity> getAllExchanges() {
        ArrayList<ExchangeEntity> exchange = new ArrayList<>();
        return new ExchangeDAO().getAllExchanges();
    }


    /* cette fonction permet de faire un échange en mettant à jours les idUser de chaque 
    Pokémon dans la BDD. Concrètement, on change les identifiants utilisateurs des Pokémons 
    dans la base de donnée afin de les "échanger".
    On vérifie par ailleurs que l'utilisateur qui souhaite valider l'échange possède bien le pokémon
    qui doit être échangé. */

    public static boolean updateIdUsers(ExchangeEntity e, int idUser){
        if(new ExchangeDAO().hasPokemonToExchange(e, idUser)){ // cas où l'utilisateur a bien le pokémon
            return new ExchangeDAO().updateIdUsers(e, idUser);
        }
        System.out.println("Impossible de faire l'échange : l'utilisateur ne possède pas le pokémon demandé dans l'échange.");
        return false;        
    }

    /* cette fonction va nous servir à créer un nouvel échange dans la BDD. On contrôle
    aussi ici les potentiels problèmes qui peuvet intervenir (doublons, pokémon qui ne 'nous'
    appartient pas, etc). */

    public static ExchangeEntity create(ExchangeEntity e) {
        if (new ExchangeDAO().hasPokemon(e) && new ExchangeDAO().isToSomeoneElse(e)){

            /* on a vérifié : 
             * que le pokémon qu'on souhaite échanger est bien à 'nous'
             * que le pokémon qu'on souhaite recevoir est bien possédé par un autre joueur (et pas 'nous')
             */

            if(!(new ExchangeDAO().isDuplicate(e))){ // vérifie si on ne va pas créer de doublons
                return new ExchangeDAO().create(e);
            }
            return null;
        }
        else{
            return null; // cas où l'utilisateur ne possède pas le pokémon à échanger
        }
    }


    public static void delete(ExchangeEntity e, int idUser){
        /* on précise qu'on ne rajoute pas de checker supplémentaire car updateIdUser sera utilisée 
        en amont, et s'occupe déjà de checker les informations entrées */
        new ExchangeDAO().delete(e);
    }

    public static ExchangeEntity getInformationExchange(int idExchange){
        return new ExchangeDAO().getInformationExchange(idExchange);
    }
}
