package com.uca.core;

import com.uca.dao.PokemonDAO;
import com.uca.entity.PokemonEntity;

import java.util.ArrayList;
import java.net.*;
import java.io.*;
import java.util.*;
import org.json.*;

public class PokemonCore {

    public static ArrayList<PokemonEntity> getPokeball(int idUser) {
        ArrayList<PokemonEntity> pokemon = new ArrayList<>();
        return new PokemonDAO().getPokeball(idUser);
    }

    public static ArrayList<PokemonEntity> getPokemonsForExchange(int idUser) {
        ArrayList<PokemonEntity> pokemon = new ArrayList<>();
        return new PokemonDAO().getPokemonsForExchange(idUser);
    }

    public static PokemonEntity getNewPokemon(int i) throws JSONException, IOException {
        // On prend un pokémon aléatoire entre 1 et 1010
        Random rand = new Random();
        int id = rand.nextInt(1011);

        URL url = new URL("https://pokeapi.co/api/v2/pokemon/" + id);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        InputStream is = conn.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer();
        while ((line = br.readLine()) != null) {
            response.append(line);
        }
        br.close();
        conn.disconnect();
        // On convertit la réponse en JSONObject
        JSONObject jsonResponse = new JSONObject(response.toString());

        // On créer le nouveau pokémon aléatoire en lui assignant les attribut name, level, idAPI
        int level = 1;
        PokemonEntity pokemonAleatoire = new PokemonEntity();
        pokemonAleatoire.setNickname("Mon " + jsonResponse.getJSONObject("species").getString("name"));
        pokemonAleatoire.setLevel(level);
        pokemonAleatoire.setIdAPI(id);
        pokemonAleatoire.setIdMaster(i);
        pokemonAleatoire.setName(jsonResponse.getJSONObject("species").getString("name"));
        //pokemonAleatoire.setType(jsonResponse.getJSONObject("types")[0].getJSONObject("type").getString("name"));
        return new PokemonDAO().create(pokemonAleatoire);
    }

    // public static String getSpecies(int id) throws JSONException, IOException {
    //     // On établit la connexion avec l'api
    //     URL url = new URL("https://pokeapi.co/api/v2/pokemon/" + id);
    //     HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    //     conn.setRequestMethod("GET");
    //     InputStream is = conn.getInputStream();
    //     BufferedReader br = new BufferedReader(new InputStreamReader(is));
    //     String line;
    //     StringBuffer response = new StringBuffer();
    //     while ((line = br.readLine()) != null) {
    //         response.append(line);
    //     }
    //     br.close();
    //     conn.disconnect();
    //     // On convertit la réponse en JSONObject
    //     JSONObject jsonResponse = new JSONObject(response.toString());

    //     // On récupère le nom de l'espèce
    //     String speciesName = jsonResponse.getJSONObject("species").getString("name");
    //     // String speciestype = jsonResponse.getJSONObject("types")[0].getJSONObject("type").getString("name");

    //     return speciesName;
    // }
}
