package com.uca.dao;

import com.uca.core.PokemonCore;
import com.uca.entity.ExchangeEntity;
import com.uca.entity.PokemonEntity;
import com.uca.dao.PokemonDAO;
import java.sql.*;
import java.util.ArrayList;
import java.sql.Statement;

public class ExchangeDAO extends _Generic<ExchangeEntity> {

    /* cette méthode récupère toutes les lignes de la table Exchange */

    public ArrayList<ExchangeEntity> getAllExchanges() {
        ArrayList<ExchangeEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange ORDER BY idUser ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ExchangeEntity entity = new ExchangeEntity();
                entity.setIdExchange(resultSet.getInt("idExchange"));
                entity.setIdUser(resultSet.getInt("idUser"));
                entity.setIdPokemon(resultSet.getInt("pokemonA"));
                entity.setIdPokemon2(resultSet.getInt("pokemonB"));
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
        return entities;
    }

    /* cette méthode vérifie si l'utilisateur qui souhaite valider l'échange a bien le pokémon que l'on souhaite échanger (idPokemon2), 
    qui est demandé par l'autre utilisateur. */

    public boolean hasPokemonToExchange(ExchangeEntity exchange, int idUser) {

        ArrayList<PokemonEntity> entities = new PokemonDAO().getPokeball(idUser); // on récupère la pokéball
        
        for (PokemonEntity entity : entities) { // recherche du pokémon en question
            if (entity.getIdPokemon() == exchange.getIdPokemon2()) {
                
                return true;
            }
        }
        
        return false;
    }

    /* cette méthode vérifie si l'utilisateur qui souhaite créer l'échange a bien le pokémon qu'il demande d'échanger. */

    public boolean hasPokemon(ExchangeEntity exchange) {
        ArrayList<PokemonEntity> entities = new PokemonDAO().getPokeball(exchange.getIdUser()); // on récupère la pokéball
        
        for (PokemonEntity entity : entities) { // recherche du pokémon en question
            if (entity.getIdPokemon() == exchange.getIdPokemon()) {
               
                return true;
            }
        }
        
        return false;
    }

    /* cette méthode vérifie si, lorsqu'un joueur fait une demande d'échange, le pokémon à échanger appartient bien à quelqu'un d'autre, à un autre
    joueur (et pas lui-même). */

    public boolean isToSomeoneElse(ExchangeEntity exchange) {
        ArrayList<PokemonEntity> entities = new PokemonDAO().getPokemonsForExchange(exchange.getIdUser()); // on récupère les pokémons des autres
        
        for (PokemonEntity entity : entities) { // recherche du pokémon en question
            if (entity.getIdPokemon() == exchange.getIdPokemon2()) { // le Pokemon2 est celui qu'on souhaite échanger
               
                return true;
            }
        }
        
        return false;
    }

    /* cette fonction vérifie qu'on ne crée par de doublons dans ls propositions d'échanges */

    public boolean isDuplicate(ExchangeEntity exchange){
        ArrayList<ExchangeEntity> entities = new ExchangeDAO().getAllExchanges();
        
        for (ExchangeEntity entity : entities) {
            if (entity.isDuplicate(exchange)) {
                return true;
            }
        }
        
        return false;
    }

    /* cette méthode se charge de l'échange de deux pokémons. Concrètement, elle change leur idUser (id du maître).
    Elle procède ainsi :
    * le pokémon 1 (celui qu'on souhaitait échanger) prend comme idUser le idUser entré (celui de la personne qui souhaite accepter l'échange).
    * le pokémon 2 (celui qu'on souhiatait recevoir) prend comme idUSer celui de l'instance exchange de ExchangeEntity.  
    Attention cependant à bien recharger les pages pokeball et pokemons_for_exchange après. */   
    public boolean updateIdUsers(ExchangeEntity exchange, int idUser){
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("UPDATE Pokemon SET idUser = ? WHERE idPokemon = ?;");
            preparedStatement.setInt(1, exchange.getIdUser());
            preparedStatement.setInt(2, exchange.getIdPokemon2());
            preparedStatement.executeUpdate();

            preparedStatement = this.connect.prepareStatement("UPDATE Pokemon SET idUser = ? WHERE idPokemon = ?;");
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, exchange.getIdPokemon());
            preparedStatement.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false; 
        }
    }

    /* cette méthode récupère les infos d'un échange à partir de son identifiant */

    public ExchangeEntity getInformationExchange(int idExchange){
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM exchange WHERE idExchange = ?;"); // une seule ligne car id unique
            preparedStatement.setInt(1, idExchange);
            ResultSet resultSet = preparedStatement.executeQuery();
    
            if (resultSet.next()) { // vérifie si un enregistrement a été trouvé
                ExchangeEntity entity = new ExchangeEntity();
                entity.setIdExchange(resultSet.getInt("idExchange"));
                entity.setIdUser(resultSet.getInt("idUser"));
                entity.setIdPokemon(resultSet.getInt("pokemonA"));
                entity.setIdPokemon2(resultSet.getInt("pokemonB"));
    
                return entity;
            } else {
                return null; // aucun enregistrement correspondant trouvé
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null; 
        }
    }

    @Override
    public ExchangeEntity create(ExchangeEntity exchange) {

        try {
            // on essaie d'insérer un nouvel échange dans la table échange
            PreparedStatement preparedStatement = this.connect.prepareStatement("INSERT INTO Exchange(idUser, pokemonA, pokemonB) VALUES (?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, exchange.getIdUser());
            preparedStatement.setInt(2, exchange.getIdPokemon());
            preparedStatement.setInt(3, exchange.getIdPokemon2());
            preparedStatement.executeUpdate();
            
            // on récupère l'idExchange généré
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int lastInsertedId = generatedKeys.getInt(1);
                exchange.setIdExchange(lastInsertedId);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            return null; // cas où la mise à jour a échoué comme lorsque par exemple une contrainte d'intégrité est violée
            /* on s'assure ici que là màj a fonctionné pour agir en conséquence */
        }

        return exchange;
    }

    @Override
    public void delete(ExchangeEntity exchange) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("DELETE FROM exchange WHERE idExchange = ?");
            preparedStatement.setInt(1, exchange.getIdExchange());
            preparedStatement.executeUpdate();            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
