package com.uca.dao;

import java.sql.*;
import java.util.ArrayList;
import com.uca.entity.PokemonEntity;
import java.sql.Statement;

public class PokemonDAO extends _Generic<PokemonEntity> {

    /* cette méthode récupère la pokéball d'un joueur selon son idUser */

    public ArrayList<PokemonEntity> getPokeball(int idUser) {
        ArrayList<PokemonEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM Pokemon WHERE idUser = ? ORDER BY idPokemon ASC;");
            preparedStatement.setInt(1, idUser);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity entity = new PokemonEntity();
                entity.setIdPokemon(resultSet.getInt("idPokemon"));
                entity.setNickname(resultSet.getString("nickname"));
                entity.setLevel(resultSet.getInt("level"));
                entity.setIdMaster(resultSet.getInt("idUser"));
                entity.setIdAPI(resultSet.getInt("idAPI"));
                entity.setName(resultSet.getString("name"));

                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entities;
    }


    /* cette méthode récupère les pokémons possédés par les autres joueurs (ceux qui n'ont pas
    idUser pour id) */

    public ArrayList<PokemonEntity> getPokemonsForExchange(int idUser) {
        ArrayList<PokemonEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM Pokemon WHERE NOT(idUser = ?) ORDER BY idUser ASC;");
            preparedStatement.setInt(1, idUser);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity entity = new PokemonEntity();
                entity.setIdPokemon(resultSet.getInt("idPokemon"));
                entity.setNickname(resultSet.getString("nickname"));
                entity.setLevel(resultSet.getInt("level"));
                entity.setIdMaster(resultSet.getInt("idUser"));
                entity.setIdAPI(resultSet.getInt("idAPI"));
                entity.setName(resultSet.getString("name"));
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entities;
    }

    @Override
    public PokemonEntity create(PokemonEntity pokemon) {
               
        Connection connection = _Connector.getInstance();
        try {
            PreparedStatement preparedstatement;
            preparedstatement = connection.prepareStatement("INSERT INTO Pokemon(idAPI, nickname, level, name, idUser) VALUES(?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            preparedstatement.setInt(1, pokemon.getIdAPI());
            preparedstatement.setString(2, pokemon.getNickname());
            preparedstatement.setInt(3, pokemon.getLevel());
            preparedstatement.setString(4, pokemon.getName());
            preparedstatement.setInt(5, pokemon.getIdMaster());
            preparedstatement.executeUpdate();

            // Récupère les clés générées
            ResultSet generatedKeys = preparedstatement.getGeneratedKeys();

            // Vérifie si des clés ont été générées
            if (generatedKeys.next()) {
                // Récupère la valeur de la première colonne (ici l'identifiant généré)
                int id = generatedKeys.getInt(1);
                pokemon.setIdPokemon(id);

                // Récupère le nom de la colonne correspondant à la clé générée
                ResultSetMetaData metadata = generatedKeys.getMetaData();
                String generatedColumnName = metadata.getColumnName(1);

            } 

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pokemon;
    }

    /* cette méthode pexe un pokémon */

    public int upgradePokemon(int id) {
        int upgradeCompleted = 0;     
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("UPDATE Pokemon SET level = level + 1 WHERE idPokemon = ?;");
            preparedStatement.setInt(1, id);
            upgradeCompleted = preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return upgradeCompleted;
    }

    /* cette méthode change le nom d'un pokémon */

    public int updateNickname(int id, String nickname, int idUser) {
        int updateCompleted = 0; 
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("UPDATE Pokemon SET nickname = ? WHERE idPokemon = ? AND idUser = ?;");
            preparedStatement.setString(1, nickname);
            preparedStatement.setInt(2, id);
            preparedStatement.setInt(3, idUser);
            updateCompleted = preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCompleted;
    }

    @Override
    public void delete(PokemonEntity p) {
    
        Connection connection = _Connector.getInstance();
        try {
            PreparedStatement preparedstatement;
            preparedstatement = connection.prepareStatement("DELETE FROM Pokemon WHERE idPokemon = ?");
            preparedstatement.setInt(1, p.getIdPokemon());
            preparedstatement.executeUpdate();
    
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
