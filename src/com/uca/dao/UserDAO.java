package com.uca.dao;

import com.uca.core.PokemonCore;
import com.uca.entity.PokemonEntity;
import com.uca.entity.UserEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.*;
import org.json.*;

public class UserDAO extends _Generic<UserEntity> {

    /* cette méthode récupère tous les utilisateurs */

    public ArrayList<UserEntity> getAllUsers() {
        ArrayList<UserEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM user ORDER BY idUser ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = new UserEntity();
                entity.setIdUser(resultSet.getInt("idUser"));
                entity.setFirstName(resultSet.getString("firstname"));
                entity.setLastName(resultSet.getString("lastname"));
                entity.setEmail(resultSet.getString("email"));
                
                // Convertir la date SQL en objet Date Java
                java.sql.Timestamp sqlTimestamp = resultSet.getTimestamp("firstConnection");
                Date javaDate = new Date(sqlTimestamp.getTime());

                entity.setFirstConnection(javaDate);

                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
        return entities;
    }

    /* cette méthode récupère les infos d'un utilisateur grâce à son idUser */

    public UserEntity getUserById (int id){
        UserEntity entity = new UserEntity();

        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM user where idUser = ?;");
            preparedStatement.setInt(1, id);
            // preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                entity.setIdUser(id);
                entity.setFirstName(resultSet.getString("firstname"));
                entity.setLastName(resultSet.getString("lastname"));
                entity.setEmail(resultSet.getString("email"));
                entity.setPassword("password");
                
                // Convertir la date SQL en objet Date Java
                java.sql.Timestamp sqlTimestamp = resultSet.getTimestamp("firstConnection");
                Date javaDate = new Date(sqlTimestamp.getTime());

                entity.setFirstConnection(javaDate);
            } else {
                return null;
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
        return entity;
    }

    /* cette méthode trouve un utilisateur selon son mot de passe et son email (unique dans la BDD)
    et elle s'assure aussi que le mot de passe entré correspond bien à celui entré pour un email
    dans la BDD. */

    public int findByEmailAndPassword(String email, String password){
        int id;
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT idUser, password FROM user where email = ?;");
            preparedStatement.setString(1, email);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                id = resultSet.getInt("idUser");

                if(!(password.equals(resultSet.getString("password")))){
                    return -2; /* cas où le mot de passe est faux */
                }
            } else {
                return -1; /* cas où on n'a pas trouvé l'email dans la BDD */
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            return -10;
        }
    
        return id;
    }

    /* cette méthode regarde l'écart entre la date de connexion d'un joueur et la date actuelle 
    en prenant compte des heures et minutes etc. Elle renvoie true si cet écart est supérieur à 24h
    et false sinon. */

    boolean timeBetweenDates(Timestamp last_co){
        Timestamp now = new Timestamp(System.currentTimeMillis());
    
        // Calcul de la différence en millisecondes entre les deux dates
        long diffInMs = now.getTime() - last_co.getTime();

        // Calcul de la différence en secondes
        long diffInSeconds = diffInMs / 1000;
    
        if (diffInSeconds >= 60 * 60 * 24) { /* différence de 24 h ou plus */
            return true;  
        } else {
            return false;
        }
    }
    
    /* cette méthode donne à l'utilisateur un nouveau pokémon si celui-ci n'en a jamais reçu ou en a reçu un pour la 
    dernière fois il y a plus de 24h. Elle renvoie true si cela a fonctionné*/
    
    public boolean getDailyPokemon(int idUser){
        try {
            PreparedStatement checkDate = this.connect.prepareStatement("SELECT firstConnection FROM user WHERE idUser = ?");
            checkDate.setInt(1, idUser);
            ResultSet result = checkDate.executeQuery();
            if (result.next()) {
                Timestamp last_co = result.getTimestamp("firstConnection");
             
                if(timeBetweenDates(last_co)){ /* cas où le dernier pokémon reçu date d'il y a 24h ou plus */
                    PokemonEntity p = PokemonCore.getNewPokemon(idUser);

                    PreparedStatement updateDate = this.connect.prepareStatement("UPDATE user SET firstConnection = NOW() WHERE idUser = ?"); // on change la date de denier pokémon reçu
                    updateDate.setInt(1, idUser);
                    updateDate.executeUpdate();

                    return true;
                } else{
                    return false;
                }

            } else { /* cas où aucun utilisateur correspondant n'est trouvé */
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public UserEntity create(UserEntity obj) {
        
        try {
            obj.setFirstConnectionToday();

            // Vérifier si un utilisateur avec le même email existe déjà
            PreparedStatement checkEmailStmt = this.connect.prepareStatement("SELECT COUNT(*) AS email_count FROM User WHERE email = ?");
            checkEmailStmt.setString(1, obj.getEmail());
            ResultSet emailCountResult = checkEmailStmt.executeQuery();
            emailCountResult.next();
            int emailCount = emailCountResult.getInt("email_count");

            if (emailCount > 0) {
                // Si un utilisateur avec le même email existe déjà, ne pas procéder à l'insertion
                return null;
            }

            PreparedStatement preparedstatement = this.connect.prepareStatement("INSERT INTO User(firstname, lastname, email, password, firstConnection) VALUES (?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            preparedstatement.setString(1, obj.getFirstName());
            preparedstatement.setString(2, obj.getLastName());
            preparedstatement.setString(3, obj.getEmail());
            preparedstatement.setString(4, obj.getPassword());
            preparedstatement.setTimestamp(5, new java.sql.Timestamp(obj.getFirstConnection().getTime()));
            preparedstatement.executeUpdate();
    
            ResultSet rs = preparedstatement.getGeneratedKeys();
            if (rs.next()) {
                obj.setIdUser(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public void delete(UserEntity obj) {
        try{
            PreparedStatement preparedStatement = this.connect.prepareStatement("DELETE FROM User WHERE idUser = ?");
            preparedStatement.setInt(1, obj.getIdUser());
            preparedStatement.executeUpdate();            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
