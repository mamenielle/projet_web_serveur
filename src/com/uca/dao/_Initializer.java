package com.uca.dao;

import java.sql.*;

import com.uca.dao.PokemonDAO;

public class _Initializer {

    public static void Init(){
        Connection connection = _Connector.getInstance();

        try {
            PreparedStatement statement;

            // Initialisation de la table User

            // firstConnexion correspond à la dernière fois qu'un utilisateur a reçu un pokémon
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS User (idUser int PRIMARY KEY AUTO_INCREMENT, firstname varchar(50), lastname varchar(50), email varchar(50), password varchar(50), firstConnection timestamp);");
            statement.executeUpdate();

            // Initialisation de la table Pokemon
            
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Pokemon (idPokemon int PRIMARY KEY AUTO_INCREMENT, idUser int REFERENCES User (idUser), idAPI int, name varchar(50), nickname varchar(50), level int, CONSTRAINT infCent CHECK (level < 100));");
            statement.executeUpdate();

            // Initialisation de la table Exchange

            // PokemonA correspond au pokémon que l'utlisateur souhaite échanger et PokemonB à celui qu'il souhaite récupérer
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Exchange (idExchange int PRIMARY KEY AUTO_INCREMENT, idUser int REFERENCES User(idUser), pokemonA int REFERENCES Pokemon(idPokemon), pokemonB int REFERENCES Pokemon(idPokemon));") ;
            statement.executeUpdate() ;


        } catch (Exception e){
            System.out.println(e.toString());
            throw new RuntimeException("could not create database !");
        }
    }
}
