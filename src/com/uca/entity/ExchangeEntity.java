package com.uca.entity;

public class ExchangeEntity {
    private int idExchange; // id de l'échange généré automatiquement par la BDD
    private int idUser; // id de l'utilisateur qui fait la demande
    private int idPokemon; // id Pokémon qui va être échangé
    private int idPokemon2; // Pokemon que l'on souhaite recevoir

    public ExchangeEntity(){
        // passer
    }

    public ExchangeEntity(int idUser, int idPokemon, int idPokemon2){ 
        this.idUser = idUser;
        this.idPokemon = idPokemon;
        this.idPokemon2 = idPokemon2;
    }

    public ExchangeEntity(int idUser, int idExchange){
        this.idUser = idUser;
        this.idExchange = idExchange;
    }

    public int getIdExchange() {
        return idExchange;
    }
    
    public void setIdExchange(int idExchange) {
        this.idExchange = idExchange;
    }
    
    public int getIdUser() {
        return idUser;
    }
    
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    
    public int getIdPokemon() {
        return idPokemon;
    }
    
    public void setIdPokemon(int idPokemon) {
        this.idPokemon = idPokemon;
    }
    
    public int getIdPokemon2() {
        return idPokemon2;
    }
    
    public void setIdPokemon2(int idPokemon2) {
        this.idPokemon2 = idPokemon2;
    }

    /* La fonction isDuplicate permet de vérifier qu'on n'a pas de doublons dans la table Exchange 
    (les ids peuvent être différents, quelconques ; on vérifie seulement si les informations entrées ne sont pas
    les mêmes). *Donc à ne pas confondre avec la méthode equals* */
    public boolean isDuplicate(ExchangeEntity e){
        return this.idUser == e.getIdUser() && this.idPokemon == e.getIdPokemon() && this.idPokemon2 == e.getIdPokemon2();
    }

}

