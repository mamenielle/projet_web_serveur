package com.uca.entity;

import java.util.ArrayList;
import java.sql.Timestamp;


public class PokemonEntity{
    private int idPokemon; // identifiant unique du Pokémon (automatique)
    private int idAPI; // contient les infos sur le pokémon : espèce et type.
    private int idMaster; // id de son maître
    private String name; // son nom (espèce)
    private String nickname; // son surnom
    private int level; // son expérience
    
    public PokemonEntity() {
        // passer
    }

    public int getIdPokemon(){ 
        return idPokemon;
    }

    public void setIdPokemon(int idPokemon){ 
        this.idPokemon = idPokemon;
    }

    public int getIdAPI() {
        return idAPI; 
    }

    public void setIdAPI(int idAPI){ 
        this.idAPI = idAPI;
    }

    public int getIdMaster(){ 
        return idMaster;
    }

    public void setIdMaster(int idMaster){ 
        this.idMaster = idMaster;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getNickname() {
        return nickname;
    }
    
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
