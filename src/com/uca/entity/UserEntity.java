package com.uca.entity;

import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.Date;
import com.uca.entity.PokemonEntity;

public class UserEntity {
    private int idUser; // identifiant unique (généré automatiquement)
    private String firstname; // prénom
    private String lastname; // nom
    private String email; // email
    private String password; // mdp
    private Date firstConnection; // date où l'utilisateur a reçu son dernier Pokémon

    public UserEntity() {
        // passer
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword(){
        return password;
    }
    
    public void setPassword(String password){
        this.password = password;
    }

    public Date getFirstConnection(){
        return firstConnection;
    }
    
    public void setFirstConnection(Date date){
        this.firstConnection = date;
    }

    /* mettre la dernière fois où l'on a reçu un pokémon à aujourd'hui */
    public void setFirstConnectionToday(){
        Date currentDate = new Date();
        this.firstConnection = currentDate;
    }
}
