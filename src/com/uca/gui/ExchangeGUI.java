package com.uca.gui;

import com.uca.entity.ExchangeEntity;
import com.uca.core.ExchangeCore;

import com.uca.gui._FreeMarkerInitializer;
import com.uca.core.UserCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class ExchangeGUI {
    public static String loadExchangeForm() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
        
        Map<String, Object> input = new HashMap<>();
    
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchange/exchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);
    
        return output.toString();
    }

    public static String getExchangeList() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
        
        Map<String, Object> input = new HashMap<>();
        // ArrayList e = ExchangeCore.getAllExchanges();
        input.put("exchanges", ExchangeCore.getAllExchanges());
        // input.put("users", ExchangeCore.getUsersExchange(e));
    
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("exchangeList/exchangeList.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);
    
        return output.toString();
    }
}
