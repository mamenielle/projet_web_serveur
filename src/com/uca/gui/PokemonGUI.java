package com.uca.gui;

import com.uca.core.PokemonCore;
import com.uca.core.PokemonCore;
import com.uca.gui._FreeMarkerInitializer;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class PokemonGUI {

    /* cette méthode donne accès aux pokémons du joueur d'identifiant idUser */

    public static String getPokeball(int idUser) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("pokemons", PokemonCore.getPokeball(idUser));

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokeball/pokeball.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    /* cette méthode donne accès aux pokémons possédés par les autres joueurs (autre que celui
    d'identifiant idUser) */

    public static String getPokemonsForExchange(int idUser) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("pokemons_for_exchange", PokemonCore.getPokemonsForExchange(idUser));

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("pokemons_for_exchange/pokemons_for_exchange.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}
