package com.uca.gui;

import com.uca.core.UserCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class UserGUI {

    /* cette methode affiche tous les utilisateurs */

    public static String getAllUsers() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        input.put("users", UserCore.getAllUsers());

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("users/users.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    /* cette methode affiche le formulaire d'inscription */

    public static String loadForm() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
           
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("register/register.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode affiche le formulaire de connexion */

    public static String loadFormLogIn() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
           
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("login/login.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }


    /* cette methode affiche la page de déconnexion */

    public static String loadFormLogOut() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
         
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("logout/logout.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette méthode permet de signaler à un utilisateur qu'il est déjà connecté */

    public static String loadAlreadyLogged() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
           
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("logged/logged.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode affiche le formulaire pour ameliorer un pokémon */

    public static String loadFormUpgrade() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("upgrade/upgrade.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode affiche le formulaire pour renommer un pokémon */

    public static String loadFormNickname() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("nickname/nickname.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode affiche "c'est un succès" */

    public static String displaySuccess() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("success/success.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode indique qu'il y a eu echec de l'opération */

    public static String displayFail() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("fail/fail.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode signale à l'utilisateur que l'améiloration de son pokémon a fonctionné */

    public static String displaySuccessUpgrade() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("successUpgrade/successUpgrade.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode signale à l'utilisateur que l'amélioration de son pokémon n'a pas fonctionné */

    public static String displayFailUpgrade() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("failUpgrade/failUpgrade.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode signale à l'utilisateur que le renommage de son pokémon a fonctionné */

    public static String displaySuccessUpdate() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("successUpdate/successUpdate.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

    /* cette methode signale à l'utilisateur que le renommage de son pokémon n'a pas fonctionné */

    public static String displayFailUpdate() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();
            
        Writer output = new StringWriter();
        Template template = configuration.getTemplate("failUpdate/failUpdate.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(null, output);
    
        return output.toString();
    }

}
