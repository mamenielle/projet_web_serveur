<#ftl encoding="utf-8">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Page d'accueil</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<body>
    <div class="container">
        <h1>Bienvenue sur notre site Pokémon !</h1>
        <p>Vous pouvez accéder aux fonctionnalités en utilisant la barre de navigation.</p>

        <h2>Connexion</h2>
        <p>N'hésitez pas à vous inscrire ou vous connecter pour profiter de nos fonctionnalités.</p>
        <a href ="register"class="style1">S'inscrire</a>
        <br>
        <a href ="login"class="style1">Se Connecter</a>
        <br>
        <a href ="logout"class="style1">Se Déconnecter</a>

        <p>Déjà connecté ? Accédez au contenu ci-dessous.</p>
        
        <h2>Pokéball et améliorations</h2>
        
        <ul>
            <li><a href ="pokeball">Voir ma Pokeball</a></li> 
            <li><a href="upgrade">Améliorer un Pokémon</a></li> 
            <li><a href="nickname">Renommer mon Pokémon</a></li> 
        </ul>

        <h2>Echanges</h2>
        <p>Echangez aussi vos pokémons contre ceux des autres joueurs <a href="exchange">ici</a>.<br>
        Consultez la liste des échanges <a href="exchangeList">ici</a>.<br>
        <a href ="pokemons_for_exchange">Voir les Pokémons des autres</a></p> 

        <br>
        <a href ="users"class="style1">Voir tous les utilisateurs</a>

        <p>Tips : Toutes les 24h, un nouveau Pokémon vous est offert pour vos loyaux services. Récupérez-le en vous connectant.
        Vous pourrez le trouver dans votre Pokéball.</p>
    </div>

</body>
</html>
