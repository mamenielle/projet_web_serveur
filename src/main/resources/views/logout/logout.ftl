<#ftl encoding="utf-8">
<head>
    <title>Se connecter</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
<body>
  <div class="container">
      <h1>Déconnexion</h1>
      <p>Besoin de vous déconnecter ou de déconnecter le dernier utilisateur ? Cliquez sur le bouton.</p>
      <br>
      <form method="post" action="/logout">
        <input type="submit" value="Se déconnecter" class="btn">
      </form>
  </div>
  </body>
</html>
