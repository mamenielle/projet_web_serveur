<#ftl encoding="utf-8">
<head>
    <title>Surnom</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
<body>
  <div class="container">
    <h1>Renommer un Pokémon</h1>
    <form method="post" action="/nickname">
      <label for="idPokemon">Identifiant du Pokémon souhaité : </label>
      <input type="text" id="idPokemon" name="idPokemon" required>
      <br>
      <label for="newNickname">Surnom souhaité : </label>
      <input type="text" id="newNickname" name="newNickname" required>
      <br>
      <input type="submit" value="Valider" class="btn">
    </form>
  </div>
  </body>
</html>