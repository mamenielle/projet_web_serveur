<#ftl encoding="utf-8">
<head>
    <title>Pokémons échangeables</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
<body>
    <div class="container">
        <ul>
            <#list pokemons_for_exchange as pokemon>
                <li>Identifiant du Pokémon : ${pokemon.getIdPokemon()}</li>
                <li>Identifiant du Pokémon (API) : ${pokemon.getIdAPI()} </li>
                <li>Identifiant de son maître : ${pokemon.getIdMaster()} </li>
                <li>Nom du Pokémon : <strong>${pokemon.getName()}</strong> </li>
                <li>Surnom du Pokémon : ${pokemon.getNickname()} </li>
                <li>Expérience : ${pokemon.getLevel()}</li>
                <br>
                <hr>
                <br>
            </#list>
        </ul>
    </div>
</body>
</html>