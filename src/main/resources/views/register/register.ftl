<#ftl encoding="utf-8">
<head>
    <title>S'inscrire</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<html xmlns="http://www.w3.org/1999/html">
<body>
  <div class="container">
      <h1>Inscription</h1>
      <form method="post" action="/register">
        <label for="firstname">Prénom :</label>
        <input type="text" id="firstname" name="firstname" required>
        <br>
        <label for="lastname">Nom :</label>
        <input type="text" id="lastname" name="lastname" required>
        <br>
        <label for="email">Email :</label>
        <input type="email" id="email" name="email" required>
        <br>
        <label for="password">Mot de passe :</label>
        <input type="password" id="password" name="password" required>
        <br>
        <input type="submit" value="S'inscrire" class="btn">
      </form>
  </div>
  </body>
</html>
