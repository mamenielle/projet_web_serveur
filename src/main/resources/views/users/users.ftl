<#ftl encoding="utf-8">
<head>
    <title>Succès</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="icon" type="image/png" href="pokeball.png">
</head>
<body xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <ul>
            <#list users as user>
                <li>Identifiant Utilisateur : ${user.getIdUser()}</li>
                <li><#if user.getFirstName()??>
                         Prénom : ${user.getFirstName()}
                    <#else>
                        Pas de prénom renseigné.
                    </#if>
                    <br>
                    <#if user.getLastName()??>
                        Nom : ${user.getLastName()}
                    <#else>
                        Pas de nom renseigné.
                    </#if>
                </li>
                <li><#if user.getEmail()??>
                    Email: ${user.getEmail()}
                <#else>
                    Pas d'email renseignée.
                </#if></li>
                <li><#if user.getFirstConnection()??>
                    Dernière connexion: ${user.getFirstConnection()?string["dd MMM yyyy HH:mm"]}
                <#else>
                    Pas d'information sur ce champ.
                </#if></li>
                <br>
                <hr>
                <br>
            </#list>
            
        </ul>
    </div>
</body>

</html>
